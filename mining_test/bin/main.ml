open Lwt.Infix
open Data_encoding
open Blake2

(* Define a type for blocks, which consists of a contents field and a nonce field. The nonce
 * is used in the proof-of-work algorithm to generate a valid block. *)
type block = {
  contents: string;
  nonce: int64;
}

let block_to_string {contents; nonce} =
    Printf.sprintf "[%s] [%s]" contents (Int64.to_string nonce)

let block_encoding =
  let open Data_encoding in
  conv
    (fun {contents; nonce} -> (contents, nonce))
    (fun (contents, nonce) -> {contents; nonce})
    (obj2
       (req "contents" string)
       (req "nonce" int64))

let separate_encoding contents nonce = 
    Bytes.cat (contents) (Binary.to_bytes_exn int64 nonce)

let block_to_bytes =
    Binary.to_bytes block_encoding

let block_of_bytes = 
    Binary.of_bytes block_encoding

let hash_digest block_bytes =
    Blake2b.direct block_bytes 32

(*TODO: Replace this with tzerror*)
let write_error_to_string e = 
    let open Binary in
    match e with 
    | Size_limit_exceeded -> "Size_limit_exceeded"
    | No_case_matched -> "No_case_matched"
    | Invalid_int { min; v; max } -> Printf.sprintf "Invalid_int: min: %d, v: %d, max: %d" min v max
    | Invalid_float { min; v; max } -> Printf.sprintf "Invalid_float: min: %f, v: %f, max: %f" min v max
    | Invalid_bytes_length { expected; found} -> Printf.sprintf "Invalid_bytes_length Expected:%d, Found:%d" expected found
    | Invalid_string_length { expected ; found } -> Printf.sprintf "Invalid_string_length Expcted:%d, Found:%d" expected found
    | Invalid_natural -> "Invalid_natural"
    | List_invalid_length -> "List_invalid_length"
    | Array_invalid_length -> "Array_invalid_length"
    | Exception_raised_in_user_function a -> Printf.sprintf "Exception_raised_in_user_function %s" a
    
let z_to_hex v= 
    let z_as_hex_string = Z.format "%x" v in
    if String.length z_as_hex_string > 64 then
        raise (Failure "Z is out of range")
    else
        `Hex (
            String.make (64 - String.length z_as_hex_string) '0' ^ z_as_hex_string
        )


let block_to_hash block =
    let block_bytes = (match block_to_bytes block with
    | Ok a -> a
    | Error e -> (
        raise (Failure (Printf.sprintf "\nBlock:%s\nError:%s" (block_to_string block) (write_error_to_string e)))
    )
    ) in

    hash_digest block_bytes


let hash_as_bytes = function Blake2b.Hash a -> a 

let block_to_hash_hex b = 
    let bytes = (b |> block_to_hash |> hash_as_bytes) in
    match Hex.of_bytes bytes with
    | `Hex a -> a

let double_hash b = 
    b |> block_to_hash |> hash_as_bytes |> hash_digest |> hash_as_bytes |> Hex.of_bytes |> Hex.show

let double_hash_faster block_bytes_without_nonce nonce = 
    separate_encoding block_bytes_without_nonce nonce |> hash_digest |> hash_as_bytes |> hash_digest |> hash_as_bytes |> Hex.of_bytes |> Hex.show

let double_hash_bytes b = 
    b |> block_to_hash |> hash_as_bytes |> hash_digest |> hash_as_bytes 

let double_hash_bytes_faster block_bytes_without_nonce nonce = 
    separate_encoding block_bytes_without_nonce nonce |> hash_digest |> hash_as_bytes |> hash_digest |> hash_as_bytes 

let bytes_to_string b= b |> Hex.of_bytes |> (function `Hex a  -> a)

(* Define a function for mining a block with a given nonce, which involves hashing the
 * block and checking if the resulting hash satisfies a certain condition (in this case,
 * if the first three characters of the hash are "000"). If a valid block is found,
 * return it as a Some value. Otherwise, return None. *)
let mine_block block nonce target : int64 option Lwt.t =
    let hash = double_hash_bytes {block with nonce} in
  if Bytes.compare hash target <= 0 then
    Lwt.return (Some nonce)
  else
    Lwt.return None

let mine_block_loop (block: block) target (start, finish) _worker_id: block option Lwt.t =
    let rec loop b nonce =
        if nonce >= finish then
            Lwt.fail (Failure "")
        else
        mine_block b nonce target >>= (fun (res) ->
            match res with
            | Some nonce -> Lwt.return (Some {block with nonce})
            | None -> loop b (Int64.succ nonce)
        )
    in
    (loop block start)

let get_range s e succ =
    let rec aux a =
        if a == e then []
        else a :: aux (succ a)
    in
    aux s

let succ n = n + 1

let partition_load workers =
    let open Int64 in
    let chuck_size = Int64.div max_int workers in
    let rec aux i =
        if i < workers then
            let start = Int64.mul i chuck_size in
            let finish = Int64.sub (Int64.add chuck_size start) 1L in
                (start, finish) :: aux (Int64.succ i)
        else
            []
        in
    aux 0L;;


let rec pick waiters =
  match waiters with
  | [] -> Lwt.return_none
  | hd :: tl ->
      Lwt.bind hd (fun v -> Lwt.return_some v)
      >>= function
      | Some v -> Lwt.return v
      | None -> pick tl



(* Define a function for starting the worker threads and running the mining tasks. *)
let mine_blocks block target workers =
    let tasks = List.mapi (fun i rng -> mine_block_loop block target rng i) (partition_load workers) in
    Lwt.pick tasks  >>= fun a ->
    (*List.iter Lwt.cancel tasks;*)
    Lwt.return a



let new_nbits_hex_to_target nbits =
    let nbits_as_bytes = `Hex nbits |> Hex.to_bytes in
    let exponent = Bytes.get_int8 (Bytes.sub nbits_as_bytes 0 1) 0  in
    let v_as_hex = "0x" ^ ((Bytes.sub nbits_as_bytes 1 ((Bytes.length nbits_as_bytes) - 1)) |> Hex.of_bytes |> Hex.show)  in
    let v = Z.of_string v_as_hex in
    
    let res = Z.mul v (Z.pow (Z.of_int 2) (8 * (exponent - 3))) |> Z.format "%x" in
    let res_left_pad = (String.make (64 - String.length res) '0') ^ res  in
    `Hex res_left_pad |> Hex.to_bytes

let stabilization first_target target_time =
    let loop current_target =
        let start_time = Unix.time () in
        let target_as_bytes = current_target |> z_to_hex |> Hex.to_bytes in
        Lwt_io.flush_all () >>= fun () ->
        Lwt_io.printf "Current target %s" (current_target |> z_to_hex |> Hex.show) >>= fun () ->
        mine_blocks {contents="Beans"; nonce=0L}  target_as_bytes 200L >>= fun b ->
            (match b with
            | Some a -> (Lwt_io.printf "Nonce: %s| Hash %s" (Int64.to_string a.nonce) (double_hash a))
            | None -> Lwt_io.printf "Error") >>= fun () ->
        let end_time = Unix.time () in
        let time_elapsed = end_time -. start_time in
        let deviation = target_time/.time_elapsed in
        let deviation_int = int_of_float deviation in
        let next_target = Z.div (current_target) (Z.of_int deviation_int) in
        Lwt_io.printf "\nStart time %f | End time %f | Time elapsed %f | Deviation %f | Deviation_int %d | Next_target %s" start_time end_time time_elapsed deviation deviation_int (next_target |> z_to_hex |> Hex.show) in
    loop first_target


let () = Lwt_main.run (
    stabilization ("0x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" |> Z.of_string) (60. *. 10.)
)


(*

let nbits_to_target nbits  =
    let nbits_as_bytes = Data_encoding.Binary.to_bytes_exn int32 nbits in
    Lwt_io.printf "\nnbits_as_bytes %s"  (bytes_to_string nbits_as_bytes) >>= fun () ->

    let exponent_bytes = Bytes.sub nbits_as_bytes 0 1 in
    Lwt_io.printf "\nexponent_bytes %s"  (bytes_to_string exponent_bytes) >>= fun () ->

    let exponent = Bytes.get_int8 exponent_bytes 0 in
    Lwt_io.printf "\nexponent %d"  exponent >>= fun () ->

    let coefficient_bytes = Bytes.sub nbits_as_bytes 1 3 in
    Lwt_io.printf "\ncoefficient_bytes %s" (bytes_to_string coefficient_bytes) >>= fun () ->

    let coefficient_hex = Hex.of_bytes coefficient_bytes |> function `Hex a -> a in
    Lwt_io.printf "\ncoefficient_hex %s" coefficient_hex >>= fun () ->

    let coefficient_hex_paded = coefficient_hex ^ String.make ((exponent - 3) * 8 / 4)  '0'  in
    Lwt_io.printf "\ncoefficient_bytes_paded %s" coefficient_hex_paded >>= fun () ->

    let coefficient_hex_left_padded = String.make (64 - String.length coefficient_hex_paded) '0' ^ coefficient_hex_paded in
    Lwt_io.printf "\ncoefficient_bytes_left_paded %s" coefficient_hex_left_padded >>= fun () ->
    let final_as_bytes = Hex.to_bytes (`Hex coefficient_hex_left_padded) in
    Lwt_io.printf "\nFinal_as_bytes %s" (bytes_to_string final_as_bytes) >>= fun () ->
    Lwt.return final_as_bytes
     
let nbits_hex_to_int h = 
    let as_bytes = (`Hex h) |> Hex.to_bytes in
    Lwt.return (Bytes.get_int32_be as_bytes 0)
*)
